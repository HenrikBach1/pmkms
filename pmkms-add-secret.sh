#!/bin/bash
FILE=pmkms-add-secret.sh
VERSION=0.01.001

###############################################################################
# This program encrypts and store manually entered contentinto a file named 
# key_id $1
###############################################################################


###############################################################################
# Encrypt and store manually entered content for key_id
###############################################################################
do_encrypt_and_store_key()
{
    local key_id=$1

    # IFS= read -p "Enter secret: " -rs secret && printf "\n" && zip -e -FI $key_id.sh.zip.pmkms <(echo $secret) && unset secret
    IFS= read -p "Enter secret: " -rs secret && printf "\n" && zip -e -FI $key_id.pmkms <(echo $secret) && unset secret
}

###############################################################################
# Main Script Execution
###############################################################################

# $1 = key_id
do_encrypt_and_store_key $1
