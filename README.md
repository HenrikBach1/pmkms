Welcome to the first release of a Poor Man's Key Management System - PMKMS.

PMKMS is released under GNU GENERAL PUBLIC LICENSE Version 3 license. Please, read COPYING.

It is built on top of linux and requires Bash, Zip and disk access, and executes only as bash scripts.

The current disk and its filesystem is the vault for the system.

Currently it only supports three basic (and almost atomic) actions: add (store/save), list (show), and get (retrieve):
* pmkms-add-secret, stores a manually entered secret for a given key,
* pmkms-list-secrets, lists the keys stored in the current working folder.
* pmkms-get-secret, retrieves the secret of a given key, and

To use PMKMS in daily duties and rutines:

```bash
$ pmkms-add-secret.sh mykey
Enter secret: 
Enter password: 
Verify password: 
	zip warning: Reading FIFO (Named Pipe): /dev/fd/63
  adding: dev/fd/63 (stored 0%)
$ pmkms-list-secrets.sh 
-rw-rw-r-- 1 myagent myagent 201 Feb 28 10:20 mykey.pmkms
...
$ # Secure, due to no information is revealed to proc-space during runtime...
$ echo $(pmkms-get-secret.sh mykey) | mysecret_consumer --stdin 
...
$ # Unsecure, if the pipe breaks during processing...
$ mysecret=$(pmkms-get-secret.sh mykey) && mysecret_consumer $mysecret && unset mysecret
[mykey.pmkms] dev/fd/63 password: 
...
$ # Unsecure, due to the secret is shown i proc-space during runtime...
$ mysecret_consumer $(pmkms-get-secret.sh mykey)
[mykey.pmkms] dev/fd/63 password: 
...
```

Contributions such as PR's, suggestions, help, etc. are appreciated under above given license terms.

NOTE further, that contributors implicitly give their consent to use their work when submitting 
contributions to this repository.

If you are not the original author/contributor of your contribution, please DON'T submit it, 
unless you have an explicitly written consent from the original author embedded in your contribution. 
Otherwise, your contribution will be rejected.

ALL RIGHTS RESERVED.

POSTAMBLE

This software is an attribution of and to the Open Source Community and its followers and 
contributors, large and small. Without them, no one were where we are as of today.

I became inspired by these ideas, when developing the first version:
* https://www.netmeister.org/blog/passing-passwords.html
* https://stackoverflow.com/questions/48098218/bash-execute-content-of-variable-including-pipe
* https://superuser.com/questions/325504/howto-pipe-cp-tar-gzip-without-creating-intermediary-files
* https://unix.stackexchange.com/questions/439497/is-there-a-way-to-pass-sensitive-data-in-bash-using-a-prompt-for-any-command
* https://www.computerhope.com/unix/bash/read.htm
* https://stackoverflow.com/questions/8467424/echo-newline-in-bash-prints-literal-n

Thank you all for sharing your ideas and knowledge with me and others.

Henrik Bach
